import React, { Component } from "react";
import Header from "../components/Header";
import FeaturedImages from "../components/MobileRoom/FeaturedImages";
import Axios from "axios";
import "./Hotel.css";
import HotelReviews from "../components/MobileRoom/HotelReviews";
import HotelLocation from "../components/MobileRoom/HotelLocation";
import Landmarks from "../components/MobileRoom/Landmarks";
import OrangeSearchBar from "../components/OrangeSearchBar";
import Footer from "./Footer";
import { Spin, Button } from "antd";
import ScrollTop from "react-scrolltop-button";
import { IoIosArrowUp, IoIosArrowBack, IoIosCalendar } from "react-icons/io";

import { AiOutlineLoading3Quarters } from "react-icons/ai";
import DesktopFeaturedImages from "../components/HotelDesktop/DesktopFeaturedImages";
import RoomsAndRates from "../components/HotelDesktop/RoomsAndRates";
import moment from "moment";

const commas = x => {
  return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

export default class MobileRoom extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      loading: true,
      selectedToken: "",
      selectedPrice: "",
      selectedCurrency: "",
      paymentData : {},
    };
  }

  componentDidMount = async () => {
    let response = await Axios.get(
      `https://cors-anywhere.herokuapp.com/https://b2c.prod-env.vervotech.com/api/hotels/${this.props.match.params.slug}/content`
    );
    let hotel = response.data.hotel;
    this.setState({ data: hotel, loading: false });
    window.scrollTo(0, 0);
  };

  handleSelection = (token, price, currency) => {
    this.setState({
      selectedToken: token,
      selectedPrice: price,
      selectedCurrency: currency,
    });
  };

  setPaymentData = (data) => {
    this.setState({ paymentData: data })
  }

  completePayment = () => {
    this.props.history.push({
      pathname: '/completePayment',
      state: {
        data: this.state.paymentData
      }
    })
  }

  render() {
    if (this.state.loading) {
      return (
        <div
          style={{
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
            height: "100vh",
          }}
        >
          <Spin size="large" />
        </div>
      );
    }

    return (
      <React.Fragment>
        {/* Mobile */}
        <div className="HotelMobileContainer">
          <Header signIn={false} fixed={true} />
          <ScrollTop
            icon={<IoIosArrowUp />}
            className="scrollToTopClass"
            text={<IoIosArrowUp />}
          />
          <div style={{ height: "9vh" }} />
          <div className="backAndDate" style={{justifyContent: 'space-between'}}>
            <IoIosArrowBack
              color="white"
              size="4rem"
              onClick={this.props.history.goBack}
            />
            <div className="backAndDate" style={{marginLeft: "auto", marginRight:'1rem' }}>
              <span>{moment(this.props.location.state.startDate).format("D MMM")} -{" "}
              {moment(this.props.location.state.endDate).format("D MMM")}</span>
              <IoIosCalendar color="white" size="4rem" style={{marginLeft:"1rem"}}/>
            </div>
          </div>

          <FeaturedImages images={this.state.data.images} />
          <div
            style={{
              fontSize: "1.5rem",
              margin: "1rem",
              marginBottom: "0.5rem",
            }}
          >
            {this.state.data.name}
          </div>
          <HotelReviews
            type={this.state.data.type}
            starRating={this.state.data.starRating}
          />
          <HotelLocation
            name={this.state.data.name}
            geoCode={this.state.data.geoCode}
            address={this.state.data.contact.address}
          />
          <Landmarks nearByAttractions={this.state.data.nearByAttractions} />
          <RoomsAndRates
            hotelId={this.props.match.params.slug}
            handleSelection={this.handleSelection}
            selected={this.state.selectedToken}
            setPaymentData={this.setPaymentData}
          />
        </div>

        {/* Desktop */}
        <div className="HotelDesktopContainer">
          <Header fixed={true} />
          <ScrollTop
            icon={<IoIosArrowUp />}
            className="scrollToTopClass"
            text={<IoIosArrowUp />}
          />
          <div style={{ height: "9vh" }} />
          <DesktopFeaturedImages images={this.state.data.images} />

          <div className="HoteldataContainer">
            <OrangeSearchBar
              history={this.props.history}
              backgroundColor="#f48247"
              top="9vh"
              place={
                this.props.location.state && this.props.location.state.place
              }
              startDate={
                this.props.location.state && this.props.location.state.startDate
              }
              endDate={
                this.props.location.state && this.props.location.state.endDate
              }
              reset={true}
            />

            <div className="mainData">
              <div
                style={{
                  fontSize: "3rem",
                  margin: "1rem",
                  marginBottom: "0.5rem",
                }}
              >
                {this.state.data.name}
              </div>
              <HotelReviews
                type={this.state.data.type}
                starRating={this.state.data.starRating}
              />
              <HotelLocation
                name={this.state.data.name}
                geoCode={this.state.data.geoCode}
                address={this.state.data.contact.address}
              />
              <Landmarks
                nearByAttractions={this.state.data.nearByAttractions}
              />
              <RoomsAndRates
                hotelId={this.props.match.params.slug}
                handleSelection={this.handleSelection}
                selected={this.state.selectedToken}
                setPaymentData={this.setPaymentData}
              />
            </div>
          </div>

          <Footer />
        </div>

        {this.state.selectedToken && (
          <div className="bookNow">
            Total: {commas(this.state.selectedPrice)} {this.state.selectedCurrency}{" "}
            <Button size="large" type="primary" style={{ marginLeft: "2vw" }} onClick={this.completePayment}>
              Book Now
            </Button>
          </div>
        )}
      </React.Fragment>
    );
  }
}
