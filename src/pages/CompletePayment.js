import React, { Component } from 'react'
import JwtDecode from "jwt-decode";
import Axios from 'axios';
import { render } from '@testing-library/react';

export default class CompletePayment extends Component {

    constructor(props){
        super(props)
        this.state = {
            loading: true,
            message: "Completing Payment, do not refresh."
        }
    }


    componentDidMount = async () => {
        let userToken = localStorage.getItem('usertoken')
        if(!userToken || !this.props.location) {
            this.setState({
                message: "Please Login to continue",
                loading: false,
            })
            
            return
        }

        try {
            JwtDecode(userToken)
    
            let referenceData = await Axios.post("/payment/getReference", {
                token: localStorage.usertoken,
                data: this.props.location.state.data
            })

            
            
            if(referenceData.data.status === "success") {
                this.setState({
                    loading: false,
                    ...referenceData.data.data
                })

                document.getElementById("ePaymentForm").submit()
            }
            
            return

        } catch (e) {
            this.setState({
                message: "Please Login to continue",
                loading: false,
            })
            
            return
        }

    }

    render = () => {


            return (
                <div>
                    {this.state.message}
                    <form method="post" id="ePaymentForm" action="https://payment.ipay88.com.my/ePayment/entry.asp">
						<input type="hidden" name="MerchantCode" value={this.state.MerchantCode} />
						<input type="hidden" name="RefNo" value={this.state.RefNo} />
						<input type="hidden" name="Amount" value={this.state.Amount} />
						<input type="hidden" name="Currency" value={this.state.Currency} />
						<input type="hidden" name="ProdDesc" value={this.state.ProdDesc} />
						<input type="hidden" name="UserName" value={this.state.UserName} />
						<input type="hidden" name="UserEmail" value={this.state.UserEmail} />
						<input type="hidden" name="UserContact" value={this.state.UserContact} />
						<input type="hidden" name="SignatureType" value={this.state.SignatureType} />
						<input type="hidden" name="Signature" value={this.state.Signature}/>
						<input type="hidden" name="ResponseURL" value={this.state.ResponseURL} />
						<input type="hidden" name="BackendURL" value={this.state.BackendURL} />
					</form>
                </div>
            )
        
    }   
}

