import RoomComponent from "../components/RoomComponent";
import React, { useEffect, useState } from "react";
import OrangeSearchBar from "../components/OrangeSearchBar";
import Header from "../components/Header";
import Footer from "./Footer";
import "./RoomFinal.css";
import { Select, Slider, InputNumber } from "antd";
import { Checkbox } from "antd";
import Filter from "../components/Filter";
import MobileLocation from "../components/MobileLocation";
import Check_box_grid from "../components/Checkbox_component";

import ScrollTop from "react-scrolltop-button";

import { IoIosArrowUp, IoIosArrowDown, IoIosArrowBack } from "react-icons/io";
const { Option } = Select;

const defaultCheckedList = ["1 star", "2 star", "3 star", "4 star", "5 star"];

const Rooms = (props) => {
  const [currency, setCurrency] = useState(
    localStorage.getItem("currency") || "MYR"
  );
  const changeCurrency = (val) => {
    localStorage.setItem("currency", val);
    setCurrency(val);
  };

  let callCount = 0;
  let perPageContent = 50;

  if (!localStorage.getItem("currency")) {
    changeCurrency("MYR");
  }

  const [hotel, setHotel] = useState([]);
  const [filter, setFilter] = useState(
    localStorage.getItem("filter") ? localStorage.getItem("filter") : "ph"
  );
  const [inProgress, setProgress] = useState(true);
  const [search, setSearch] = useState(false);
  const [minHotelPrice, setMin] = useState(0);
  const [maxHotelPrice, setMax] = useState(100);

  const [minFilterPrice, setFilterMin] = useState(minHotelPrice);
  const [maxFilterPrice, setFilterMax] = useState(maxHotelPrice);
  const [seeFilters, setseeFilters] = useState(false);

  const [CheckboxStars, setCheckboxStars] = useState([]);
  const [CheckboxPopular, setCheckboxPopular] = useState([]);
  // const [CheckboxOptions, setCheckboxOptions] = useState([]);

  const [totalHotel, setTotalHotel] = useState("");
  const hotelHolder = {};
  const guid = () => {
    function s4() {
      return Math.floor((1 + Math.random()) * 0x10000)
        .toString(16)
        .substring(1);
    }
    return (
      s4() +
      s4() +
      "-" +
      s4() +
      "-" +
      s4() +
      "-" +
      s4() +
      "-" +
      s4() +
      s4() +
      s4()
    );
  };
  const header = {
    "Content-Type": "application/json; charset=utf-8",
    "Accept-Encoding": " gzip, deflate",
    "customer-ip": "49.37.192.211",
    correlationId: guid(),
    accountId: "nexus-demo-account",
    currency,
  };
  const getLocation = () => {
    let place = props.location.state.place;
    let code;

    return new Promise((resolve, reject) => {
      fetch(
        `https://nexus.prod-env.vervotech.com/api/locations/locationcontent/autosuggest?term=${place}&countries=${code}`,
        {
          method: "GET",
        }
      ).then((res) => {
        res.json().then((place) => {
          console.log(place.locationSuggestions[0].id);
          // response will be getting the subcoordinates of the search place with checkIn checkOut date
          fetch(
            `https://nexus.prod-env.vervotech.com/api/locations/locationcontent/location/${place.locationSuggestions[0].id}?getSublocations=true`,
            {
              method: "GET",
            }
          ).then((location) => resolve(location.json()));
        });
      });
    });
  };
  const getStaticData = (opts) => {
    fetch(
      "https://nexus.prod-env.vervotech.com/api/content/hotelcontent/getHotelContent",
      {
        method: "POST",
        body: JSON.stringify(opts),
        headers: header,
      }
    )
      .then((res) => res.json())
      .then(async (staticData) => {
        let firstLoad = staticData.hotels.slice(0, perPageContent);

        console.log("first-load-list", firstLoad);

        setHotel(firstLoad);
        staticData.hotels.map((item) => {
          hotelHolder[item.id] = item;
        });

        console.log("staticHotel", hotelHolder);
      })
      .catch((e) => console.log(e));
  };
  const initCall = (opts) => {
    return new Promise((resolve, reject) => {
      fetch("https://nexus.prod-env.vervotech.com/api/hotel/availability/init", {
        method: "POST",
        body: JSON.stringify(opts),
        headers: header,
      }).then((res) => resolve(res.json()));
    });
  };
  const recursiveCall = (count, responseToken, nextResultsKey) => {
    let url = `https://nexus.prod-env.vervotech.com/api/hotel/availability/async/${responseToken.token}/results`;
	console.log(url)
    if (nextResultsKey) {
      url += `?nextResultsKey=${nextResultsKey}`;
    }

    return fetch(url, {
      method: "GET",
      headers: header,
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        return data;
      })
      .catch((error) => {
        console.log(error);
      });
  };

  const processHotel = async (data) => {
    let updated = false;
    data.hotels &&
      data.hotels.map((item) => {
        if (typeof hotelHolder[item.id] !== "undefined") {
          hotelHolder[item.id] = { ...hotelHolder[item.id], ...item };
        }
      });
  };

  const getHotel = (responseToken, nextResultsKey = null) => {
    recursiveCall(callCount, responseToken, nextResultsKey).then(
      async (data) => {
        processHotel(data);

        if (data.status == "InProgress") {
          if (typeof data.nextResultsKey != "undefined") {
            nextResultsKey = data.nextResultsKey;
          }

          getHotel(responseToken, nextResultsKey);
        } else {
          if (data.status == "Completed") {
            const newstaticHotel = {};
            Object.values(hotelHolder).map((item, index) => {
              if (item.rate != null) {
                return (newstaticHotel[item.id] = item);
              }
            });

            console.log("complete-list", newstaticHotel);
            console.log("complete-list", newstaticHotel.starRating);
            console.log(
              "total props ",
              props.location.state.place,
              Object.keys(newstaticHotel).length
            );
            setTotalHotel(Object.keys(newstaticHotel).length);

            // Hotels Loaded
            setHotel(Object.values(newstaticHotel));

            let max = Math.max.apply(
              Math,
              Object.values(newstaticHotel).map((o) => {
                return o.rate.totalRate;
              })
            );
            let min = Math.min.apply(
              Math,
              Object.values(newstaticHotel).map((o) => {
                return o.rate.totalRate;
              })
            );
            setMax(max);
            setMin(min);

            setFilterMax(max);
            setFilterMin(min);

            setProgress(false);
          }

          return data;
        }
      }
    );
  };

  const onChangeRange = (value) => {
    setFilterMax(value[1]);
    setFilterMin(value[0]);
  };
//ChannelID: "vd-demo-channel",
  const getHotelData = () => {
    const checkInDate = props.location.state.startDate;
    const checkOutDate = props.location.state.endDate;
    const occupancies = props.location.state.occupancies;

    getLocation().then((coordinates) => {
      let opts = {
        channelId: "vd-demo-channel",
        currency,
        culture: "en-US",
        checkIn: checkInDate,
        checkOut: checkOutDate,
        occupancies,
        nationality: "IN",
        countryOfResidence: "IN",
        polygonalRegion: {
          coordinates: coordinates.boundaries[0],
        },
      };

      getStaticData(opts);
      initCall(opts).then((responseToken) => {
        console.log("Hotel Token", responseToken);
        localStorage.setItem("inittoken", responseToken.token);
        localStorage.setItem("correlationId", header.correlationId);
        getHotel(responseToken);
      });
    });
  };

  const sort_by = (field, reverse, primer) => {
    const key = primer
      ? function (x) {
          return primer(x[field]);
        }
      : function (x) {
          return x[field];
        };

    reverse = !reverse ? 1 : -1;

    return function (a, b) {
      return (a = key(a)), (b = key(b)), reverse * ((a > b) - (b > a));
    };
  };

  const sort_by_price = (reverse) => {
    const key = function (x) {
      return parseFloat(x.rate.totalRate);
    };

    reverse = !reverse ? 1 : -1;

    return function (a, b) {
      return (a = key(a)), (b = key(b)), reverse * ((a > b) - (b > a));
    };
  };
  const handleFilter = (value) => {
    setFilter(value);
  };

  const handleSearch = () => {
    setSearch(!search);
  };




  useEffect(() => {
    setProgress(true);
    getHotelData();
  },[
    props.location.state.place,
    currency,
    props.location.state.startDate,
    props.location.state.endDate,
  ]);


  // brings scroll to top on every search
  useEffect(() => {
    window.scrollTo(0, 0);
  }, []);

  let filteredHotel = [];
  let filtersObject = {}
  let hotelType = {}
  let options = {}
  if (!inProgress){
      //make an object with all filter values

    //finds the filter values ie 5star (15), 4star (100) ...
    filtersObject = {
      star1: 0,
      star2: 0,
      star3: 0,
      star4: 0,
      star5: 0,
      freeBreakfast: 0,
      payAtHotel: 0,
    };

    options = {}
    hotelType = {};

    for (let singleHotel in hotel) {
      let starRating = hotel[singleHotel].starRating;

      if (starRating < 2.0) {
        filtersObject["star1"]++;
      } else if (starRating < 3.0) {
        filtersObject["star2"]++;
      } else if (starRating < 4.0) {
        filtersObject["star3"]++;
      } else if (starRating < 5.0) {
        filtersObject["star4"]++;
      } else if (starRating == 5.0) {
        filtersObject["star5"]++;
      }

      if (hotel[singleHotel].options) {
        if (hotel[singleHotel].options.freeBreakfast) {
          filtersObject["freeBreakfast"]++;
        }

        if (hotel[singleHotel].options.payAtHotel) {
          filtersObject["payAtHotel"]++;
        }
      }

      
      // to count Hotel, Apartment...
      if (hotel[singleHotel].type) {
        if(hotelType.hasOwnProperty(hotel[singleHotel].type)){
          hotelType[hotel[singleHotel].type]++;
        } else {
          hotelType[hotel[singleHotel].type] = 1;
        }
      }

      if (hotel[singleHotel].options) {
        for (let key of Object.keys(hotel[singleHotel].options)) {
          if (hotel[singleHotel].options[key] === true) {
            if (options.hasOwnProperty(key)) {
              options[key]++;
            } else {
              options[key] = 1;
            }
          }
        }
      }

    }



  }
  if (!inProgress) {
    //Stars filter, they will "or" with other star filters and "and" with the other filters
    CheckboxStars.forEach((val) => {
      switch (val) {
        case "star1":
          filteredHotel = [
            ...filteredHotel,
            ...hotel.filter(
              (val) => val.starRating < 2.0 && val.starRating >= 1.0
            ),
          ];
          break;
        case "star2":
          filteredHotel = [
            ...filteredHotel,
            ...hotel.filter(
              (val) => val.starRating < 3.0 && val.starRating >= 2.0
            ),
          ];
          break;
        case "star3":
          filteredHotel = [
            ...filteredHotel,
            ...hotel.filter(
              (val) => val.starRating < 4.0 && val.starRating >= 3.0
            ),
          ];
          break;
        case "star4":
          filteredHotel = [
            ...filteredHotel,
            ...hotel.filter(
              (val) => val.starRating < 5.0 && val.starRating >= 4.0
            ),
          ];
          break;
        case "star5":
          filteredHotel = [
            ...filteredHotel,
            ...hotel.filter((val) => val.starRating == 5),
          ];
          break;
      }
    });

    if (CheckboxStars.length == 0) {
      //incase no star filters are selected
      filteredHotel = hotel; 
    }

    
    CheckboxPopular.forEach((filterName) => {
      switch (filterName) {
        case "payAtHotel":
        case "freeBreakfast":
        case "freeCancellation":
          filteredHotel = filteredHotel.filter(
            (hotelInfo) =>
              hotelInfo.options && hotelInfo.options[filterName] === true
          );
          break;


        default:
          filteredHotel = filteredHotel.filter(
            (val) => val.type === filterName
          );
      }
    });

    filteredHotel = filteredHotel.filter((hotel) => {
      return (
        hotel.rate.totalRate >= minFilterPrice &&
        hotel.rate.totalRate <= maxFilterPrice
      );
    });
  }

  if (inProgress) {
    filteredHotel = hotel;
  } else if (filter === "sh") {
    filteredHotel = filteredHotel.sort(sort_by("starRating", true, parseFloat));
    console.log("Filter hotel star rating wise", filteredHotel); //[15081076].starRating
  } else if (filter === "pl") {
    filteredHotel = filteredHotel.sort(sort_by_price(false));
  } else if (filter === "ph") {
    filteredHotel = filteredHotel.sort(sort_by_price(true));
  }

  let totalFilteredHotels = filteredHotel.length;
  filteredHotel = filteredHotel.slice(0, perPageContent);

  

  let seeFilterOption = {
    style: { display: "none" },
  };

  if (seeFilters) {
    seeFilterOption = {
      style: { display: "flex" },
    };
  } else {
    seeFilterOption = {
      style: { display: "none" },
    };
  }

  return (
    <div className="roomFinalMain">
      <Header signIn={false} fixed={true} />
      <ScrollTop
        icon={<IoIosArrowUp />}
        className="scrollToTopClass"
        text={<IoIosArrowUp />}
      />
      <div style={{ height: "8vh" }} />
      <div className="mobileSearch">
        <IoIosArrowBack
          color="white"
          size="4rem"
          onClick={props.history.goBack}
        />
        <MobileLocation
          place={props.location.state.place}
          search={search}
          handleSearch={handleSearch}
          startDate={props.location.state.startDate}
          endDate={props.location.state.endDate}
        />
      </div>
      <section className="roomslist">
        <div className="desktopSearch">
          <div>
            <div>
              <OrangeSearchBar
                backgroundColor="#f48244"
                place={props.location.state.place}
                startDate={props.location.state.startDate}
                endDate={props.location.state.endDate}
                history={props.history}
                noofAdults={props.location.state.noofAdults}
                noofChild={props.location.state.noofChild}
                noofroom={props.location.state.noofroom}
                top={"9vh"}
                reset={true}
                className={`${search ? "elementShow" : "elementHide"}`}
              />
            </div>
            <div
              className="FilterControl"
              onClick={() => setseeFilters(!seeFilters)}
            >
              <div>Filters: </div>
              <div>{seeFilters ? <IoIosArrowUp /> : <IoIosArrowDown />}</div>
            </div>
            <div className="Filtertext"> Filters : </div>
            {/* <div style={{width: "23.5vw", margin: "0 2rem",border: "double", 
      "border-radius": "2rem","color": "red","padding-left": "2rem"}}> */}
            {/* <div className="budgettext">Budget: </div> */}
            <div className={`filterOutline ${seeFilters ? "elementShow" : ""}`}>
              <div className="slidermaincontainer">
                <div className="sliderfilterclassmain">
                  <div>
                    <Slider
                      min={minHotelPrice}
                      max={maxHotelPrice}
                      range={true}
                      value={[minFilterPrice, maxFilterPrice]}
                      onChange={onChangeRange}
                      className="sliderStyle"
                    />
                  </div>
                  <div>
                    <InputNumber
                      value={minFilterPrice}
                      onChange={(val) => setFilterMin(val)}
                    ></InputNumber>

                    <InputNumber
                      className="buttonslider"
                      value={maxFilterPrice}
                      onChange={(val) => setFilterMax(val)}
                    ></InputNumber>
                  </div>
                </div>
              </div>
              <Filter handleFilter={handleFilter} />
              <Check_box_grid
                filtersObject={filtersObject}
                CheckboxPopular={CheckboxPopular}
                setCheckboxPopular={setCheckboxPopular}
                setCheckboxStars={setCheckboxStars}
                CheckboxStars={CheckboxStars}
                hotelType={hotelType}
                options={options}
                // CheckboxOptions={CheckboxOptions}
                // setCheckboxOptions={setCheckboxOptions}
              />

              <div className="fliterMainContainer" {...seeFilterOption}>

                {/* <div className="filterTop desktopCurrency" /> */}
                <div className="filterSelect desktopCurrency">
                  <Select
                    value={currency}
                    onChange={(value) => {
                      changeCurrency(value);
                    }}
                  >
                    <Option key="MYR">Currency: MYR</Option>
                    <Option key="USD">Currency: USD</Option>
                    <Option key="EUR">Currency: EUR</Option>
                    <Option key="INR">Currency: INR</Option>
                    <Option key="AED">Currency: AED</Option>
                    <Option key="PHP">Currency: PHP</Option>
                    <Option key="BHD">Currency: BHD</Option>
                    <Option key="SAR">Currency: SAR</Option>
                    <Option key="OMR">Currency: OMR</Option>
                    <Option key="QAR">Currency: QAR</Option>
                  </Select>
                </div>
              </div>
              <div></div>
            </div>
          </div>
        </div>

        <div className="roomUIList">
          {totalFilteredHotels > 0 && (
            <div style={{ fontSize: "2rem" }}>
              {" "}
              {props.location.state.place}: {totalFilteredHotels} Properties
              found{" "}
            </div>
          )}
          <hr />

          {/* {inProgress &&<Spin size="large" />} */}
          <div className="roomfinalcontainer">
            {filteredHotel.map((item, index) => {
              return (
                <RoomComponent
                  history={props.history}
                  place={props.location.state.place}
                  startDate={props.location.state.startDate}
                  endDate={props.location.state.endDate}
                  key={item.id}
                  {...item}
                />
              );
            })}
          </div>
        </div>
      </section>
      <Footer />
    </div>
  );
};

export default Rooms;
