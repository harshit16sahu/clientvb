import React, { Component } from 'react';
import axios from "axios";
import { MDBAlert } from "mdbreact";
import { BrowserRouter as Link } from 'react-router-dom';
import './LoginDhruva.css';


export default class LoginDhruva extends Component {
    constructor(props) {
		super(props);

		if (localStorage.usertoken) {
			this.props.history.push(`/`);
		}
		this.state = {
            name:"",
            phone:"",
			email: "",
			password: "",
            passwordConfirm:true,
			errors: [],
			registered: false
		};
	}

	handleEmail = event => {
		this.setState({
			email: event.target.value
		});
	};

	handlePassword = event => {
		this.setState({
			password: event.target.value
		});
	};

	handleSubmit = event => {
		event.preventDefault();
// ---------------------------------
// this.props.history.push(`/`);	
// --------------------------------
		try {
			axios
				.post("users/login", {
					email: this.state.email,
					password: this.state.password
				})
				.then(response => {
					console.log("ress - " + response.message);

					if (response.data === "Error: Wrong Email/Password") {
						console.log("Login failed");
						let arr = [];
						arr.push(response.data);

						this.setState({
							errors: arr
						});
					} else if (response.data) {
						console.log(response.data.message);
						localStorage.setItem("usertoken", response.data.token);
						this.props.history.push(`/`);
					}
				})
				.catch(err => {
					console.log(err);
				});
		} catch (e) {
			console.log("Error in Login");
			console.log(e);
		}
	};

    handleNameSignUp = event => {
		this.setState({
			name: event.target.value
		});
	};

	handleEmailSignUp = event => {
		this.setState({
			email: event.target.value
		});
	};

	handlePassword = event => {
		this.setState({
			password: event.target.value
		});
	};

	handlePasswordConfirm = event => {
		if(event.target.value === this.state.password) {
			this.setState({
				passwordConfirm: true
			});
		}else {
			this.setState({
				passwordConfirm: false
			});
		}
	}

	handleSubmitSignUp = event => {
		event.preventDefault();
		console.log("Signup Details - ");
		console.log("Name: " + this.state.name);
		console.log("Email: " + this.state.email);
		console.log("Password: " + this.state.password);	
		// ---------------------------------
		// this.props.history.push(`/login`);	
		// ---------------------------------
		try {
			axios
				.post("users/register", {
					name: this.state.name,
					email: this.state.email,
					password: this.state.password
				})
				.then(response => {
					console.log(response.data);

					if (response.data === "Success: Registration successful") {
						console.log("Registered");

						// this.props.history.push(`/login`)
						this.setState({ registered: true });
					} else if (response.data === "Error: User already exists") {
						let arr = [];
						arr.push(response.data);

						this.setState({
							errors: arr
						});

						// TODO : Give the users some message //
					}
				});
		} catch (e) {
			console.log("Error in Signup");
			console.log(e);
		}
    };
    
    render() {

		let success = false;

		if (this.props.location.state && this.props.location.state.success) {
			success = true;
		}

		const successMessage = (
			<MDBAlert color="success">Registration successful</MDBAlert>
		);

		const err = (
			<MDBAlert color="danger">
				{this.state.errors.map((error, i) => (
					<p key={i}>{error}</p>
				))}
			</MDBAlert>
		);
        return (
            <div className="d-md-flex h-md-100 align-items-left">
                {this.state.errors.length !== 0 ? (
								err
							) : (
								<div style={{ height: "5vh" }} />
							)}
                <div className="col-md-6 p-0 bg-white h-md-100  ">
                    <div className="d-md-flex align-items-left h-100 p-5 text-left justify-content-left">
                        <form>
                            <div>

                                <img src="https://www.visitorsdeals.com/static/media/logo-full.a4ea859c.png" className="img-fluid mt-1 mb-1 w-25" alt="Logo" />
                                <h1 className="mb-4 text-pink mx-5">Log In</h1>
                                </div>




                            <div className=" mx-4 my-4 form-group">
                                <label className="text-pink"  >Email Id:</label>
                                <input type="email" id="name1" onChange={this.handleEmail} className="form-control" />

                            </div>
                            <div className="mx-4 my-4 form-group">
                                <label className="text-pink">Phone Number:</label>
                                <input type="text" id="name1" className="form-control" />

                            </div>
                            <div className="mx-4 my-4 form-group">
                                <label className="text-pink">Password:</label>
                                <input type="password" id="name1" onChange={this.handlePassword} className="form-control " />
                                <small className="form-text text-muted-white">
                                    Your password must be 8-20 characters long, contain letters, numbers and special characters, but must not contain spaces.
                                 </small>
                            </div>
                            <button type="button" className="btn bg-pink float-right cus1" onClick={this.handleSubmit}>LOG IN</button>
                            <br></br>

                            <p className="text-black cor">Sign in with Social Network</p>
                            <br></br>
                            <button className="loginBtn loginBtn--facebook">
                                Login with Facebook
</button>

                            <button className="loginBtn loginBtn--google">
                                Login with Google
</button>


                        </form>

                    </div>
                </div>
                <div className="or">OR</div>
             

                <div className="col-md-6 p-0 bg-pink h-md-100 loginarea">
                    <div className="d-md-flex align-items-left h-md-100 p-5 justify-content">
                        <form >
                            <h1 className="mb-4 mx-4 p-5 text-white">Sign Up</h1>


                            <div className="mx-4 my-4 form-group">
                                <label className="text-white">Name:</label>
                                <input type="name" id="name" onChange={this.handleNameSignUp} className="form-control" />
                            </div>


                            <div className=" mx-4 my-4 form-group">
                                <label className="text-white"  >Email Id:</label>
                                <input type="email" id="name" onChange={this.handleEmail} className="form-control" />

                            </div>
                            <div className="mx-4 my-4 form-group">
                                <label className="text-white">Phone Number:</label>
                                <input type="text" id="name" className="form-control" />

                            </div>
                            <div className="mx-4 my-4 form-group">
                                <label className="text-white" >Password:</label>
                                <input type="password" id="name" onChange={this.handlePassword} className="form-control " />
                                <small className="form-text text-muted-white">
                                    Your password must be 8-20 characters long, contain letters, numbers and special characters, but must not contain spaces.
                           </small>
                            </div>


                            <button type="submit" className="btn bg-white cus"
										onSubmit={this.handleSubmitSignUp}
										>SIGN ME UP</button>
                        </form>
                    </div>
                </div>
            </div>

        );
    }
}
