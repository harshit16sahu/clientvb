import React, { Component } from "react";
import { Spin, Button } from "antd";
import Axios from "axios";
import "./RoomsAndRates.css";
import RoomPriceContainer from "./RoomPriceContainer";

export default class RoomsAndRates extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      data: [],
      loadMore: true,
      // selected: "",
    };
  }

  componentDidMount = () => {
    this.getRoomsAndRates();
  };

  getRoomsAndRates = async () => {
    let hotelId = this.props.hotelId;
    let token = localStorage.getItem("inittoken");
    let correlationId = localStorage.getItem("correlationId");

    let ipData = await Axios.get(
      "https://cors-anywhere.herokuapp.com/http://gd.geobytes.com/GetCityDetails"
    );
    console.log(ipData);
    let ip = ipData.data.geobytesipaddress;
    localStorage.setItem("ip", ip);

    let data = {
      searchSpecificProviders: false,
    };

    let config = {
      headers: {
        "Content-Type": "application/json; charset=utf-8",
        // "Accept-Encoding": "gzip, deflate",
        accountId: "nexus-demo-account",
        "customer-ip": ip,
        correlationId: correlationId,
      },
    };
    let response = await Axios.post(
      `https://nexus.prod-env.vervotech.com/api/hotel/${hotelId}/roomsandrates/${token}`,
      data,
      config
    );

    let standardizedRoomsData = {};
    let standardizedRooms = response.data.hotel.standardizedRooms;
    // standardizedRooms.config = config

    await response.data.hotel.recommendations.forEach((element) => {
      console.log(element.groupId);
      if (standardizedRoomsData.hasOwnProperty("_" + element.groupId)) {
        standardizedRoomsData["_" + element.groupId].push(element);
      } else {
        standardizedRoomsData["_" + element.groupId] = [element];
      }
    });

    console.log("sl ", standardizedRooms);
    this.setState({
      standardizedRoomsData,
      standardizedRooms,
      loading: false,
      config,
      hotelId: response.data.hotel.id,
      token: response.data.token,
      currency: response.currency,
      roomPriceContainersDisplayed: Object.keys(standardizedRoomsData).length
    });
  };

  roomPriceContainersFailedToLoad = () => {
      this.setState({ roomPriceContainersDisplayed: this.state.roomPriceContainersDisplayed - 1})
  }
  loadMore = () => {
    this.setState({ loadMore: !this.state.loadMore });
  };

  render() {

    
    if (this.state.loading) {
      return (
        <div className="loading">
          <Spin size="large" />
        </div>
      );
    } else {

        if(this.state.roomPriceContainersDisplayed <=0 ){
            return (<div style={{display:"flex", height: "30vh", width:"100%"}}>
                <div style={{margin:"auto", fontSize:"large"}}>No Rooms found. Please try another Hotel.</div>
            </div>)
        }
      let combinationCosts = [];

      for (let i in this.state.standardizedRoomsData) {
        let roomsarray;
        if (this.state.loadMore) {
          roomsarray = this.state.standardizedRoomsData[i];
        } else {
          roomsarray = this.state.standardizedRoomsData[i].slice(0, 2);
        }

        let roomData = this.state.standardizedRooms[Number(i.slice(1)) - 1];

        combinationCosts.push(
          <RoomPriceContainer
            key={i}
            hotelId={this.state.hotelId}
            token={this.state.token}
            config={this.state.config}
            rooms={roomsarray}
            handleSelection={this.props.handleSelection}
            selectedtoken={this.props.selected}
            roomData={roomData}
            roomPriceContainersFailedToLoad = {this.roomPriceContainersFailedToLoad}
            setPaymentData={this.props.setPaymentData}

          />
        );
      }
      return <div className="RoomsAndRatesContainer">{combinationCosts}</div>;
    }
  }
}
