import React, { Component } from "react";
import { Button } from "antd";
import RoomPrice from "./RoomPrice";
import Facilities from "../Facilities";

export default class RoomPriceContainer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loadMore: true,
      roomsDisplayed: props.rooms.length,
    };
  }

  roomFailedToLoad = () => {
    this.setState({ roomsDisplayed: this.state.roomsDisplayed - 1 });
    if(this.state.roomsDisplayed <=0 ){
        this.props.roomPriceContainersFailedToLoad()
    } 
  };

  loadMore = () => {
    this.setState({
      loadMore: !this.state.loadMore,
    });
  };
  render() {
    let roomsArray;
    if (this.state.loadMore) {
      roomsArray = this.props.rooms;
    } else {
      roomsArray = this.props.rooms.slice(0, 2);
    }

    console.log("rooms", this.props.rooms.length);

    let roomData = this.props.roomData;

    if (this.state.roomsDisplayed <= 0) {
      return (<div style={{display:"none"}} />)
    }
    return (
      <React.Fragment>
        <div className="combinationCostscontainer">
          <div className="combinationImageName">
            <div style={{ display: "flex", flexDirection: "column" }}>
              {roomData.images.length > 0 && (
                <div className="RoomsImageContainer">
                  {roomData.images.map((element, index) => (
                    <div
                      key={index}
                      className="RoomsImage"
                      style={{
                        backgroundImage: `url( ${element.links[0].url} )`,
                      }}
                    />
                  ))}
                </div>
              )}
              <div className="combinationCostscontainerroomName">
                {roomData.name}
              </div>
              {roomData.hasOwnProperty("facilities") &&
                roomData.facilities.length > 0 && (
                  <Facilities facilities={roomData.facilities} />
                )}
              <div className="roomsPrices">
                {roomsArray.map((element, index) => (
                  <RoomPrice
                    hotelId={this.props.hotelId}
                    token={this.props.token}
                    config={this.props.config}
                    recommendationId={element.id}
                    handleSelection={this.props.handleSelection}
                    selectedtoken={this.props.selectedtoken}
                    roomFailedToLoad={this.roomFailedToLoad}
                    setPaymentData={this.props.setPaymentData}
                  />
                ))}
              </div>
            </div>
          </div>

          <hr className="hrStyle" />
        </div>
      </React.Fragment>
    );
  }
}
